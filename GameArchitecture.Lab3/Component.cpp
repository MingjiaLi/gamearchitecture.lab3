#include "Component.h"

void Component::Initialize()
{
	Object::Initialize();
}

void Component::Update()
{
}

std::string& Component::GetComponentId()
{
	// diagram shows that component class has no members
	std::string id = std::to_string(this->GetId());
	return id;
}

Component::Component()
{
}

Component::~Component()
{
}