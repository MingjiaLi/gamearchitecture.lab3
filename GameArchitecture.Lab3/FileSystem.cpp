#include "FileSystem.h"

FileSystem& FileSystem::Instance()
{
	static FileSystem instance;
	return instance;
}

FileSystem::FileSystem()
{
}

FileSystem::FileSystem(const FileSystem&)
{
}

FileSystem::~FileSystem()
{
}

void FileSystem::Initialize()
{
}

void FileSystem::Update()
{
}

void FileSystem::Load(std::string fileName)
{
}