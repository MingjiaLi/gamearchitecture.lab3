#ifndef TEXTURE_ASSET_H
#define TEXTURE_ASSET_H

#include "Asset.h"

class TextureAsset : Asset
{
public:
	TextureAsset();
	~TextureAsset() override;
};

#endif // ! TEXTURE_ASSET_H