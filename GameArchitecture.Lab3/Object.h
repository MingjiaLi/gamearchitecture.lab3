#ifndef OBJECT_H
#define OBJECT_H

#include <string>

class Object
{
public:
	virtual void Initialize();

	bool IsInitialized() { return this->initialized; };
	std::string GetName() { return this->name; }
	int GetId() { return this->id; }

protected:
	Object();
	virtual ~Object();

private:
	bool initialized;
	std::string name;
	int id;
};

#endif // !OBJECT_H