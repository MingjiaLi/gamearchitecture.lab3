#ifndef GAME_OBJECT_H
#define GAME_OBJECT_H

#include <string>
#include <map>
#include "Component.h"

class GameObject : public Object
{
public:
	GameObject();
	~GameObject() override;

	void AddComponent(Component* component);
	void RemoveComponent(Component* component);
	void Initialize() override;
	void Update();

private:
	std::map<std::string, Component*> components;
};

#endif // !GAME_OBJECT_H