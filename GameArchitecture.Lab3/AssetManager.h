#ifndef ASSET_MANAGER_H
#define ASSET_MANAGER_H

#include <list>
#include "Asset.h"
#include "ISystem.h"

class AssetManager : public ISystem
{
public:
	static  AssetManager& Instance();
	void AddAsset(Asset* component);
	void RemoveAsset(Asset* component);

private:
	AssetManager();
	AssetManager(const AssetManager&);
	~AssetManager();

	AssetManager& operator=(const AssetManager&) = delete;

protected:
	void Initialize() override;
	void Update() override;

private:
	friend class GameEngine;
	std::list<Asset*> assets;
};

#endif // !ASSET_MANAGER_H