#ifndef COMPONENT_H
#define COMPONENT_H

#include "Object.h"

class Component : public Object
{
public:
	void Initialize() override;
	void Update();
	virtual std::string& GetComponentId();

protected:
	Component();
	~Component() override;
};

#endif // !COMPONENT_H