#ifndef I_SYSTEM_H
#define I_SYSTEM_H

class ISystem
{
protected:
	friend class GameEngine;

	virtual void Initialize() = 0;
	virtual void Update() = 0;
};

#endif // !I_SYSTEM_H