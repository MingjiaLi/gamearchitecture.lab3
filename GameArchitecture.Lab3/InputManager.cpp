#include "InputManager.h"

InputManager& InputManager::Instance()
{
	static InputManager instance;
	return instance;
}

InputManager::InputManager()
{
}

InputManager::InputManager(const InputManager&)
{
}

InputManager::~InputManager()
{
}

void InputManager::Initialize()
{
}

void InputManager::Update()
{
}