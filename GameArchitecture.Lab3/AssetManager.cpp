#include "AssetManager.h"

AssetManager& AssetManager::Instance()
{
	static AssetManager instance;
	return instance;
}

void AssetManager::AddAsset(Asset* component)
{
	this->assets.push_back(component);
}

void AssetManager::RemoveAsset(Asset* component)
{
	this->assets.remove(component);
}

AssetManager::AssetManager()
{
}

AssetManager::AssetManager(const AssetManager&)
{
}

AssetManager::~AssetManager()
{
}

void AssetManager::Initialize()
{
}

void AssetManager::Update()
{
}