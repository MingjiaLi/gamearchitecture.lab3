#ifndef ASSET_H
#define ASSET_H

#include "Object.h"

class Asset : public Object
{
protected:
	Asset();
	~Asset() override;
};

#endif // !ASSET_H