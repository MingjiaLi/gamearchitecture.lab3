#include "GameObject.h"

GameObject::GameObject()
{
}

GameObject::~GameObject()
{
	this->components.clear();
}

void GameObject::AddComponent(Component* component)
{
	this->components[component->GetComponentId()] = component;
}

void GameObject::RemoveComponent(Component* component)
{
	this->components.erase(component->GetComponentId());
}

void GameObject::Initialize()
{
}

void GameObject::Update()
{
	for (auto comp = this->components.begin(); comp != this->components.end(); ++comp)
	{
		comp->second->Update();
	}
}