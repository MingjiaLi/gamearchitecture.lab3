#ifndef INPUT_MANAGER_H
#define INPUT_MANAGER_H

#include "ISystem.h"

class InputManager : public ISystem
{
public:
	static InputManager& Instance();

private:
	InputManager();
	InputManager(const InputManager&);
	~InputManager();

	inline InputManager& operator=(const InputManager&) = delete;

protected:
	void Initialize() override;
	void Update() override;

private:
	friend class GameEngine;
};

#endif // !INPUT_MANAGER_H