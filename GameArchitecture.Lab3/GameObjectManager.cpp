#include "GameObjectManager.h"

GameObjectManager& GameObjectManager::Instance()
{
	static GameObjectManager instance;
	return instance;
}

void GameObjectManager::AddGameObject(GameObject* component)
{
	this->gameObjects.push_back(component);
}

void GameObjectManager::RemoveGameObject(GameObject* component)
{
	this->gameObjects.remove(component);
}

GameObject* GameObjectManager::FindGameObjectById(int id)
{
	for (GameObject* gameObject : this->gameObjects)
	{
		if (gameObject->GetId() == id)
		{
			return gameObject;
		}
	}

	return nullptr;
}

GameObjectManager::GameObjectManager()
{
}

GameObjectManager::GameObjectManager(const GameObjectManager&)
{
}

GameObjectManager::~GameObjectManager()
{
	for (const GameObject* gameObject : gameObjects)
	{
		delete gameObject;
		gameObject = nullptr;
	}

	this->gameObjects.clear();
}

void GameObjectManager::Initialize()
{
}

void GameObjectManager::Update()
{
	for (GameObject* gameObject : this->gameObjects)
	{
		gameObject->Update();
	}
}