#ifndef SPRITE_H
#define SPRITE_H

#include "Component.h"
#include "IRenderable.h"

class Sprite : IRenderable, Component
{
public:
	Sprite();
	~Sprite() override;
	std::string& GetComponentId() override;
	void Initialize() override;

protected:
	void Render() override;
};

#endif // !SPRITE_H