#ifndef GAME_ENGINE_H
#define GAME_ENGINE_H

class GameEngine
{
public:
	static GameEngine& Instance();
	void Initialize();
	void GameLoop();

private:
	GameEngine();
	GameEngine(const GameEngine&);
	~GameEngine();

	inline GameEngine& operator=(const GameEngine&) = delete;
};

#endif // !GAME_ENGINE_H