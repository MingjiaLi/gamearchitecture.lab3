#ifndef RENDER_SYSTEM_H
#define RENDER_SYSTEM_H

#include <string>
#include <list>
#include "IRenderable.h"
#include "ISystem.h"

class RenderSystem : public ISystem
{
public:
	static  RenderSystem& Instance();
	void AddRenderable(IRenderable* component);
	void RemoveRenderable(IRenderable* component);

private:
	RenderSystem();
	RenderSystem(const RenderSystem&);
	~RenderSystem();

	void LoadSettings();
	std::list<IRenderable*> RenderComponents();

	inline RenderSystem& operator=(const RenderSystem&) = delete;

protected:
	void Initialize() override;
	void Update() override;

private:
	friend class GameEngine;

private:
	std::string name;
	int width;
	int height;
	bool fullscreen;

	std::list<IRenderable*> renderComponents;
};

#endif // !RENDER_SYSTEM_H