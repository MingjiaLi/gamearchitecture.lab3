#include "RenderSystem.h"
#include <fstream>
#include "simpleJSON/json.hpp"

RenderSystem& RenderSystem::Instance()
{
	static RenderSystem instance;
	return instance;
}

void RenderSystem::AddRenderable(IRenderable* component)
{
	this->renderComponents.push_back(component);
}

void RenderSystem::RemoveRenderable(IRenderable* component)
{
	this->renderComponents.remove(component);
}

RenderSystem::RenderSystem() :
	name("Game Engine"),
	width(1920),
	height(1080),
	fullscreen(false)
{
}

RenderSystem::RenderSystem(const RenderSystem&) :
	name("Game Engine"),
	width(1920),
	height(1080),
	fullscreen(false)
{
}

RenderSystem::~RenderSystem()
{
	for (IRenderable* component : this->renderComponents)
	{
		delete component;
	}

	this->renderComponents.clear();
}

void RenderSystem::LoadSettings()
{
	std::ifstream inputStream("./JsonSettings/RenderSystem.json");
	const std::string str((std::istream_iterator<char>(inputStream)), std::istream_iterator<char>());
	const json::JSON& settings = json::JSON::Load(str);

	this->name = settings.at("name").ToString();
	this->width = settings.at("width").ToInt();
	this->height = settings.at("height").ToInt();
	this->fullscreen = settings.at("fullscreen").ToBool();

	std::cout << "Render System: " << std::endl;
	std::cout << "name: " << this->name << std::endl;
	std::cout << "width: " << this->width << std::endl;
	std::cout << "height: " << this->height << std::endl;
	std::cout << "fullscreen: " << this->fullscreen << std::endl;
}

std::list<IRenderable*> RenderSystem::RenderComponents()
{
	for (IRenderable* component : this->renderComponents)
	{
		component->Render();
	}

	return this->renderComponents;
}

void RenderSystem::Initialize()
{
	this->LoadSettings();
}

void RenderSystem::Update()
{
}