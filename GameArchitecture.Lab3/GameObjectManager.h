#ifndef GAME_OBJECT_MANAGER_H
#define GAME_OBJECT_MANAGER_H

#include <list>
#include "GameObject.h"
#include "ISystem.h"

class GameObjectManager : public ISystem
{
public:
	static GameObjectManager& Instance();
	void AddGameObject(GameObject* component);
	void RemoveGameObject(GameObject* component);
	GameObject* FindGameObjectById(int id);

private:
	GameObjectManager();
	GameObjectManager(const GameObjectManager&);
	~GameObjectManager();

	inline GameObjectManager& operator=(const GameObjectManager&) = delete;

protected:
	void Initialize() override;
	void Update() override;

private:
	friend class GameEngine;
	std::list<GameObject*> gameObjects;
};

#endif // !GAME_OBJECT_MANAGER_H