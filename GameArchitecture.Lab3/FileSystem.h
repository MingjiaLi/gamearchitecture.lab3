#ifndef FILE_SYSTEM_H
#define FILE_SYSTEM_H

#include <string>
#include "ISystem.h"

class FileSystem : public ISystem
{
public:
	static FileSystem& Instance();

private:
	FileSystem();
	FileSystem(const FileSystem&);
	~FileSystem();

	inline FileSystem& operator=(const FileSystem&) = delete;

protected:
	void Initialize() override;
	void Update() override;
	void Load(std::string fileName);

private:
	friend class GameEngine;
};

#endif // !FILE_SYSTEM_H