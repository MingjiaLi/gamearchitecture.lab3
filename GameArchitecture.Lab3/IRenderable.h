#ifndef I_RENDERABLE_H
#define I_RENDERABLE_H

class IRenderable
{
	friend class RenderSystem;

protected:
	IRenderable() = default;
	~IRenderable() = default;

	virtual void Render()
	{
	}
};

#endif // !I_RENDERABLE_H
