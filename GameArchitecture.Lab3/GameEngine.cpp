#include "GameEngine.h"

#include "AssetManager.h"
#include "FileSystem.h"
#include "GameObjectManager.h"
#include "InputManager.h"
#include "RenderSystem.h"

GameEngine::GameEngine()
{
}

GameEngine::GameEngine(const GameEngine&)
{
}

GameEngine::~GameEngine()
{
}

GameEngine& GameEngine::Instance()
{
	static GameEngine instance;
	return instance;
}

void GameEngine::Initialize()
{
	AssetManager::Instance().Initialize();
	FileSystem::Instance().Initialize();
	GameObjectManager::Instance().Initialize();
	InputManager::Instance().Initialize();
	RenderSystem::Instance().Initialize();
}

void GameEngine::GameLoop()
{
	while (true)
	{
		AssetManager::Instance().Update();
		FileSystem::Instance().Update();
		GameObjectManager::Instance().Update();
		InputManager::Instance().Update();
		RenderSystem::Instance().Update();
	}
}